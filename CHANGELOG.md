# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.0.1"></a>
## [1.0.1](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.22...v1.0.1) (2018-06-28)



<a name="1.0.1"></a>
## [1.0.1](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.22...v1.0.1) (2018-06-28)



<a name="1.0.1"></a>
## [1.0.1](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.22...v1.0.1) (2018-06-28)



<a name="1.1.22"></a>
## 1.1.22 (2018-06-28)


### Bug Fixes

* **config:** default db uri with app name in development ([6926fa2](https://bitbucket.org/kokengthecoder/vd_assessment/commits/6926fa2))
* **get:** remove timestamp prop in get funtion ([e122887](https://bitbucket.org/kokengthecoder/vd_assessment/commits/e122887))
* **script:** update dockerfile scripting for development and production ([bf440b5](https://bitbucket.org/kokengthecoder/vd_assessment/commits/bf440b5))


### Features

* **test:** add ava test framework and basic test passed ([cacf295](https://bitbucket.org/kokengthecoder/vd_assessment/commits/cacf295))



<a name="1.1.22"></a>
## [1.1.22](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.21...v1.1.22) (2018-06-28)



<a name="1.1.21"></a>
## [1.1.21](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.20...v1.1.21) (2018-06-28)



<a name="1.1.20"></a>
## [1.1.20](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.18...v1.1.20) (2018-06-28)



<a name="1.1.19"></a>
## [1.1.19](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.18...v1.1.19) (2018-06-28)



<a name="1.1.18"></a>
## [1.1.18](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.17...v1.1.18) (2018-06-28)



<a name="1.1.17"></a>
## [1.1.17](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.16...v1.1.17) (2018-06-28)



<a name="1.1.16"></a>
## [1.1.16](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.15...v1.1.16) (2018-06-28)



<a name="1.1.15"></a>
## [1.1.15](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.14...v1.1.15) (2018-06-28)



<a name="1.1.14"></a>
## [1.1.14](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.13...v1.1.14) (2018-06-28)



<a name="1.1.13"></a>
## [1.1.13](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.11...v1.1.13) (2018-06-28)



<a name="1.1.12"></a>
## [1.1.12](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.11...v1.1.12) (2018-06-28)



<a name="1.1.11"></a>
## [1.1.11](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.10...v1.1.11) (2018-06-28)



<a name="1.1.10"></a>
## [1.1.10](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.8...v1.1.10) (2018-06-28)



<a name="1.1.9"></a>
## [1.1.9](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.8...v1.1.9) (2018-06-28)



<a name="1.1.8"></a>
## [1.1.8](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.7...v1.1.8) (2018-06-28)



<a name="1.1.7"></a>
## [1.1.7](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.6...v1.1.7) (2018-06-28)



<a name="1.1.6"></a>
## [1.1.6](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.5...v1.1.6) (2018-06-28)



<a name="1.1.5"></a>
## [1.1.5](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.4...v1.1.5) (2018-06-28)



<a name="1.1.4"></a>
## [1.1.4](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.3...v1.1.4) (2018-06-28)



<a name="1.1.3"></a>
## [1.1.3](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.1...v1.1.3) (2018-06-28)



<a name="1.1.2"></a>
## [1.1.2](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.1...v1.1.2) (2018-06-28)



<a name="1.1.1"></a>
## [1.1.1](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.1.0...v1.1.1) (2018-06-28)



<a name="1.1.0"></a>
# [1.1.0](https://bitbucket.org/kokengthecoder/vd_assessment/compare/v1.0.0...v1.1.0) (2018-06-28)



<a name="1.0.0"></a>
# 1.0.0 (2018-06-28)


### Bug Fixes

* **config:** default db uri with app name in development ([6926fa2](https://bitbucket.org/kokengthecoder/vd_assessment/commits/6926fa2))
* **script:** update dockerfile scripting for development and production ([bf440b5](https://bitbucket.org/kokengthecoder/vd_assessment/commits/bf440b5))


### Features

* **test:** add ava test framework and basic test passed ([cacf295](https://bitbucket.org/kokengthecoder/vd_assessment/commits/cacf295))
