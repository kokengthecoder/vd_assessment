## Getting Started

### Prerequisites

Docker and docker-compose are necessary. MongoDB docker is attach in the docker-compose, there's no need to use native MongoDB to run this app in development mode.

[Docker CE Installation](https://docs.docker.com/install/)

Production App is hosted in [Heroku](https://www.heroku.com/) and mongodb is hosted at [Mongolab](https://mlab.com/)

### Development / Running Locally

Clone the repo
```
$ git clone https://kokengthecoder@bitbucket.org/kokengthecoder/vd_assessment.git
```

Start the app
```
$ docker-compose up
```

Nodemon is used to watch code changes and automatically re-run the server.

### Test

[AVA](https://github.com/avajs/ava) is used as a test framework to run separate test file parallelly.

To start the test
```
$ yarn test
```
Lint

[Standardjs](https://standardjs.com/) used for code linting

```
yarn lint
```

### Versioning

[Standard Version](https://github.com/conventional-changelog/standard-version)is used to auto code versioning
```
$ yarn release
$ yarn release:patch
$ yarn release:minor
$ yarn release:major
```

### Deployment

Setup Heroku
```
$ heroku login
$ heroku create
```

Setup Mongolab Add-on
```
$ heroku addons:create mongolab
```

Compile to the source code to dist folder
```
$ yarn build
```

Build Image and push to Heroku Container Registry
```
$ heroku container:push web
```

Deploy Image
```
$ heroku container:release web
```

### CI/CD Pipeline Integration

Test will be running in every master branch push and tag push.

Docker build and push to registry upon successfully tag pushed is currently undergoing task.

### To-do List

Please refer to my (trello board)[https://trello.com/b/2GcI6u67/vault-dragon-assessment]

