#!/bin/bash

yarn && yarn build || exit 1

docker build -f Dockerfile -t $APP_NAME . || exit 1

docker login --username=$REGISTRY_USERNAME --password=$REGISTRY_AUTH_TOKEN $PRIVATE_REGISTRY

docker tag $APP_NAME $PRIVATE_REGISTRY/$APP_NAME/web:$BITBUCKET_TAG

docker push $PRIVATE_REGISTRY/$APP_NAME/web:$BITBUCKET_TAG