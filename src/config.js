import Joi from 'joi'
import 'dotenv/config'

const debug = require('debug')('app:config')

const envSchema = Joi.object().keys({
  // General config
  NODE_ENV: Joi.string().default('development'),
  APP_NAME: Joi.string().default('app'),
  PORT: Joi.string().default('8080'),

  // MONGODB
  MONGO_URI: Joi.string().default('mongodb://mongo:27017'),
  MONGO_DBNAME: Joi.string().default(Joi.ref('APP_NAME'))
}).unknown()

const { error, value: env } = Joi.validate(process.env, envSchema)
if (error) throw error

const config = {
  appName: env.APP_NAME,
  port: env.PORT,
  db: {
    mongodb: {
      uri: env.NODE_ENV === 'development'
        ? `${env.MONGO_URI}/${env.APP_NAME}`
        : env.MONGO_URI
    }
  },
  mongooseDebug: env.MONGOOSE_DEBUG
}

debug(config)

export default config
