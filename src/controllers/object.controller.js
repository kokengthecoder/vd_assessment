import ObjectModel from '../models/object.model'
import formatTime from '../helpers/format-time'

async function post (req, res, next) {
  try {
    // extract key and value from request body
    const key = Object.keys(req.body)[0]
    const value = Object.values(req.body)[0]

    // find and create data
    const obj = await ObjectModel.findAndCreate({ key, value })

    // format to readable time
    obj.timestamp = formatTime(obj.timestamp)

    // response upon successful saved
    res.json(obj)
  } catch (e) {
    return next(e)
  }
}

async function get (req, res, next) {
  const { timestamp } = req.query
  let queryTime

  // default queryTime set to current time if not provided
  timestamp
    ? queryTime = new Date(parseInt(timestamp))
    : queryTime = new Date()

  try {
    // find data by key in params
    const obj = await ObjectModel.findByKeyAndTime(req.params.key, queryTime)

    // response found data or message if no data found
    res.json(obj || `Data doesn't exist`)
  } catch (e) {
    return next(e)
  }
}

export default {
  post,
  get
}
