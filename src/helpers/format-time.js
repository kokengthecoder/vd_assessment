function formatTime (time) {
  const hour = time.getHours()
  const minute = time.getMinutes()
  let temp = '' + ((hour > 12) ? hour - 12 : hour)
  if (hour === 0) { temp = '12' }
  temp += ((minute < 10) ? '.0' : '.') + minute
  temp += (hour >= 12) ? 'pm' : 'am'
  return temp
}

export default formatTime
