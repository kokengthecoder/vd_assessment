import config from './config'
import app from './init/express'
require('./init/mongo')

const { port } = config

app.listen(port, () => {
  console.log(`app listened at port: ${port}`)
})

export default app
