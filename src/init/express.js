import express from 'express'

// Routes
import mainRoutes from '../routes'

const app = express()

app.use(express.json())

// Get all the routes
app.use('/', mainRoutes)

// Catch error and response the error message
app.use((err, req, res, next) => {
  console.error('~~~ Unexpected error exception start ~~~')
  console.error('params:', req.params)
  console.error('query:', req.query)
  console.error('body:', req.body)
  console.error(err)
  console.error('~~~ Unexpected error exception end ~~~')

  return res.status(err.status || 500).json({ error: err.message })
})

export default app
