import mongoose from 'mongoose'
import config from '../config'

// setup mongoose to use Promise
mongoose.Promise = global.Promise

const options = {
  'keepAlive': 300000,
  'connectTimeoutMS': 30000
}

const mongoUri = config.db.mongodb.uri
mongoose.connect(mongoUri, options)
mongoose.connection.on('error', () => {
  throw new Error(`unable to connect to database: ${mongoUri}`)
})
