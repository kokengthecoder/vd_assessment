import mongoose from 'mongoose'

// construct model schema
const ObjectSchema = new mongoose.Schema({
  key: String,
  value: {}
}, {
  timestamps: { createdAt: 'timestamp', updatedAt: false }
})

// Static method
ObjectSchema.statics = {

  async findAndCreate (obj) {
    const result = await this.findOneAndUpdate(
      obj, // condition
      {}, // leave empty because not going to update it
      { upsert: true, new: true, fields: '-_id -__v', lean: true } // options
    )

    return result
  },

  async findByKeyAndTime (key, queryTime) {
    const results = await this
      .find({
        key,
        timestamp: { $lt: queryTime }
      })
      .sort({ _id: -1 })
      .select('-_id -__v -timestamp')
      .lean()

    // only select the latest from the results list
    return results[0]
  }

}

// export model
export default mongoose.model('ObjectModel', ObjectSchema)
