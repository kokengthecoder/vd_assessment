import express from 'express'
import objectRouter from './object.routes'

const router = express.Router()

router.use('/health-check', (req, res) => res.send('OK'))
router.use('/object', objectRouter)

export default router
