import express from 'express'
import objectCtrl from '../controllers/object.controller'

const router = express.Router()

router.post('/', objectCtrl.post)
router.get('/:key', objectCtrl.get)

export default router
