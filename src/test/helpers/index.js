import mongoose from 'mongoose'
import app from '../../init/express'
import ObjectModel from '../../models/object.model'

const MongodbMemoryServer = require('mongodb-memory-server').default
const mongod = new MongodbMemoryServer()

// Create connection to mongoose before all tests
exports.before = async t =>
  mongoose.connect(await mongod.getConnectionString())

// Create fixtures before each test
exports.beforeEach = async t => {
  const obj = new ObjectModel({ key: 'mykey', value: 'value1' })

  await obj.save()

  t.context.app = app
}

// Clean up database after every test
exports.afterEach = async t => ObjectModel.remove()

// Disconnect MongoDB and mongoose after all tests are done
exports.after = async t => {
  mongoose.disconnect()
  mongod.stop()
}
