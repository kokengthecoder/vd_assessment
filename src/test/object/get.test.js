import test from 'ava'
import request from 'supertest'

import { before, beforeEach, afterEach, after } from '../helpers'

test.before(before)
test.beforeEach(beforeEach)
test.afterEach.always(afterEach)

test.serial('get object', async t => {
  const { app } = t.context
  const res = await request(app)
    .get('/object/mykey')
  t.is(res.status, 200)
  t.is(res.body.key, 'mykey')
  t.is(typeof res.body.timestamp, 'undefined')
})

test.serial('get object with query timestamp', async t => {
  const { app } = t.context
  const res = await request(app)
    .get('/object/mykey')
    .query({ timestamp: (new Date()).getTime() })
  t.is(res.status, 200)
  t.is(res.body.key, 'mykey')
  t.is(typeof res.body.timestamp, 'undefined')
})

test.serial('get object with timestamp set before object creation', async t => {
  const { app } = t.context
  const res = await request(app)
    .get('/object/mykey')
    .query({ timestamp: (new Date(0)).getTime() })
  t.is(res.status, 200)
  t.is(res.body, `Data doesn't exist`)
})

test.after.always(after)
