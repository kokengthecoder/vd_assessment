import test from 'ava'
import request from 'supertest'

import { before, beforeEach, afterEach, after } from '../helpers'

test.before(before)
test.beforeEach(beforeEach)
test.afterEach.always(afterEach)

test.serial('post new object', async t => {
  const { app } = t.context
  const res = await request(app)
    .post('/object')
    .send({ mykey1: 'value1' })
  t.is(res.status, 200)
  t.is(res.body.key, 'mykey1')
  t.is(res.body.value, 'value1')
  console.log(res.body.timestamp)
  t.not(res.body.timestamp.match(/^\d{1,2}\.\d{2}(pm|am)$/), null)
})

test.serial('post existing key object', async t => {
  const { app } = t.context
  const res = await request(app)
    .post('/object')
    .send({ mykey1: 'value2' })
  t.is(res.status, 200)
  t.is(res.body.key, 'mykey1')
  t.is(res.body.value, 'value2')
})

test.after.always(after)
